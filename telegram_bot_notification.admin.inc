<?php

/**
 * @file
 * Administration interfaces for Telegram Bot notification module.
 */

/**
 * Main settings.
 */
function telegram_bot_notification_settings() {
  // How to create Telegram bot.
  $form['telegram_bot_notification']['help_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('How to create Telegram bot?'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('<p>To create your Telegram Bot, follow this instructions:</p>
                      <ol>
                        <li>Message <a href="https://telegram.me/botfather" target="_blank">@BotFather</a> with the following text: <code>/newbot</code>;</li>
                        <li>Type whatever <code>name</code> you want for your Bot;</li>
                        <li>Type whatever <code>username</code> you want for your bot, minimum 5 characters, may use «<code>_</code>» symbol, and must end with <code>bot</code>. For example: <code>mysample_bot</code> or <code>MySampleBot</code>;</li>
                        <li>Copy token to access the HTTP API, paste to «Specify access token» field and save this form;</li>
                        <li>Type <code>/setprivacy</code> and choose your bot;</li>
                        <li>Choose <code>Enable</code>, if your bot will only receive messages that either start with the «<code>/</code>» symbol or mention the bot by username. Choose <code>Disable</code>, if your bot will receive all messages that people send to groups;</li>
                        <li>To change additional parameters — type <code>/help</code> to @BotFather.</li>
                      </ol>
                      <p>If you looking for screencast, see <a href="http://blizzy.ru/node/11">this blog post</a>.</p>'),
  );

  // Specify access token.
  $form['telegram_bot_notification']['specify_access_token'] = array(
    '#type' => 'fieldset',
    '#title' => t('Specify access token'),
    '#weight' => 1,
  );
  $form['telegram_bot_notification']['specify_access_token']['access_token'] = array(
    '#type' => 'textfield',
    '#title' => 'Enter your Telegram Bot access token',
    '#default_value' => variable_get('telegram_bot_notification_access_token', ''),
    '#description' => t('<p>For example: <em>1479063:AAGYeGqFH1cqYiS1YW-waC1JKcdGwvXWDq4</em>.</p>
                      <p>Important: Go to <code>https://api.telegram.org/botYOUR-ACCESS-TOKEN/getMe</code> for test. If you see page with response, like <code>{"ok":true,"result":{...}}</code> — your Telegram Bot is ready to use.</p>'),
  );

  // Specify node type.
  $form['telegram_bot_notification']['specify_node_type'] = array(
    '#type' => 'fieldset',
    '#title' => t('Specify node type'),
    '#weight' => 3,
  );
  $form['telegram_bot_notification']['specify_node_type']['node_type'] = array(
    '#type' => 'textfield',
    '#title' => 'Enter machine name of node type',
    '#default_value' => variable_get('telegram_bot_notification_node_type', ''),
    '#description' => t('<p>Single or multiple (comma separated). For example: <em>article, page, blog_post</em>.</p>'),
  );

  // Return form.
  return system_settings_form($form);
}
