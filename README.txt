Telegram Bot notification
-------
Allows to use your own Telegram Bot for notify your registered users about 
new blog post or else.


Standard usage scenario
--------------------------------------------------------------------------------
1. Create Telegram Bot.
2. Place special bot script (bot brain) on your server (SSL required).
3. Download module, upload to server (./sites/all/modules).
4. Go to administration page (admin/modules) and enable module.
5. Go to settings page: admin/config/content/telegram_bot_notification/settings
6. Follow instructions.

Note: After install, module create field field_telegram_chat_id for every 
registered user on your site. This field must be filled by user himself.

Create Telegram Bot guide (RU lang): [1].

To use module requires placement of special bot script (bot brain) on your 
server. This server security requirements imposed, that is, you need to have SSL 
certificate for set Telegram Webhook. Example bot script (PHP) can be found here 
GitHub repo [2].


Credits / contact
--------------------------------------------------------------------------------
Currently maintained by Victor Shostak [1].

Ongoing development is sponsored by IA Central marketing [4] and Blizzy's 
Blog [5].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://github.com/enjoyiacm/telegram_bot_notification/issues


References
--------------------------------------------------------------------------------
1: http://blizzy.ru/node/11
2: http://bit.ly/1RU0Y8v
3: https://www.drupal.org/u/enjoyiacm
4: http://centralmarketing.ru
5: http://blizzy.ru
